#!/bin/bash

PROJECT=
SERVICE_ACCOUNT=
INSTANCE_NAME=
MACHINE_TYPE=
REGION=
ZONE=


## firewall rule for allowing http and https
gcloud compute --project=$PROJECT firewall-rules create default-allow-http --direction=INGRESS --priority=1000 --network=default --action=ALLOW --rules=tcp:80 --source-ranges=0.0.0.0/0 --target-tags=http-server

gcloud compute --project=PROJECT firewall-rules create default-allow-https --direction=INGRESS --priority=1000 --network=default --action=ALLOW --rules=tcp:443 --source-ranges=0.0.0.0/0 --target-tags=https-server

#### creatign the instance which download and run the contaienr image
gcloud beta compute --project=$PROJECT instances create-with-container $INSTANCE_NAME --zone=$ZONE --machine-type=$MACHINE_TYPE --subnet=default --network-tier=PREMIUM --metadata=google-logging-enabled=true --maintenance-policy=MIGRATE --service-account=$SERVICE_ACCOUNT --scopes=https://www.googleapis.com/auth/devstorage.read_only,https://www.googleapis.com/auth/logging.write,https://www.googleapis.com/auth/monitoring.write,https://www.googleapis.com/auth/servicecontrol,https://www.googleapis.com/auth/service.management.readonly,https://www.googleapis.com/auth/trace.append --tags=http-server,https-server --image=cos-stable-78-12499-59-0 --image-project=cos-cloud --boot-disk-size=10GB --boot-disk-type=pd-standard --boot-disk-device-name=INSTANCE_NAME --container-image=ilieash/techtest --container-restart-policy=always --labels=container-vm=cos-stable-78-12499-59-0 --reservation-affinity=any
