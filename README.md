# Technical Test Q-CTRL
- index.html -- static single html file
- Dockerfile -- Dockerfile to build
- ilieash/techtest -- the docker image hosted on dockerhub.
- http://techtest.friendsco.ml -- free subdomain to server the html

This is a very simple way to serve a static website through nginx.
the code includes a static "index.html" file and a docker file to include and serve that file using nginx.

Replicating this is very simple just clone the project and build the Dockerfile if you need to change any content in index.html file

or if you dont need to change anything just run it by executing docker run.

## Getting Started

### running it on local machine (Assuming docker installed)

```bash

docker run -d -p 80:80 ilieash/techtest

```
This will pull the lastest build image from the docker hub repo and run it locally and serve the static page on port 80
you can test it by using

```bash
curl localhost
```

### modifying content of index.html

if you want change the content and add more pages, just edit the index.html file and/or add more files(adding more files will need to modify the Dockerfile also to include them). 

```bash
docker build -t ilieash/techtest:v2.0 .
```

once build finish you can run it same way locally.

```bash
docker run -d -p 80:80 ilieash/techtest
```

### Runnig it on cloud
You can run this image on any cloud where docker is supported. or you could run in on a vminstance with docker installed.
#### GCLOUD
I am adding the gcloud example as i am running my instance there but as its docker this is portable and can be run anywhere. 
I am adding a simple script which create a compute instance and automatically download the docker image and serve it in a public ip.
This script is in the script folder. it also has two firewall rule but by default this rules are already in the default vpc.


# What could be done
1. First is security. this is servred as http only. this could be a tls with free cert from letsencrypt.
2. scalling. Could be deployed in a more controlled environment like kubernetes. Could be easily scalled.
3. As this is a single page, its served under the default nginx config. Can be a custom config with more options.
4. Troubleshoot section. could add a troubleshoot section for common issues.
5. capture logging and metrics.
6. set a uptime checker and notifier.
